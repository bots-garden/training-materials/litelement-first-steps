import { LitElement, html } from '@polymer/lit-element';
import {style} from './main-styles.js';

//import './my-title.js'
//import './my-sub-title.js'

import { MyTitle } from './my-title.js'
import { MySubTitle } from './my-sub-title.js'
import { BuddiesList } from './buddies-list.js'

import { channels } from './bus.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
    /*
      let event = new CustomEvent('ping', { detail: "zzzz" })
      window.dispatchEvent(event)
    */
    window.addEventListener("message", (event) => { 
      console.log(`you've got a message`, event)
    });

    // USE https://alligator.io/js/maps-introduction/
    channels.push(this)
    console.log(channels)

    /*
    console.log(this)
    console.log(JSON.stringify(this))
    console.log(customElements.get("buddies-list"))
    */

  }

  render() {
    return html`
      ${style}
      <section class="container">
        <div>
          <my-title message="👋"></my-title>
          <my-sub-title my-text="Training with 🦊 GitLab is 😍"></my-sub-title>
          <hr>
          <buddies-list></buddies-list>
        </div>
      </section>
    `
  }

}

customElements.define('main-application', MainApplication);
