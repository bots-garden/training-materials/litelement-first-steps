import { LitElement, html } from '@polymer/lit-element';
import {style} from './main-styles.js';

import { channels } from './bus.js'


export class MySubTitle extends LitElement {

  static get properties() {
    return {
        myText: { attribute: 'my-text' }
    }
  }

  constructor() {
    super()
    channels.push(this)
    console.log(channels)
    setInterval(()=> {
      this.change()
    }, 1000)
  }

  change() {
    this.myText = new Date()
    //console.log(this.myText)
  }

  render(){
    return html`
      ${style}
      <h2 class="subtitle">
        ${this.myText}
      </h2>    
    `
  }

}
customElements.define('my-sub-title', MySubTitle)