import { LitElement, html } from '@polymer/lit-element'

export class MyTitle extends LitElement {
  constructor() {
    super()
  }
  render(){
    return html`
      <style>
        .title {
          font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
          display: block;
          font-weight: 300;
          font-size: 100px;
          color: #35495e;
          letter-spacing: 1px;
        }      
      </style>
      <h1 class="title">
        Kitchen Sink 🍔 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)