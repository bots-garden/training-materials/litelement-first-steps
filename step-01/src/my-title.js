import { LitElement, html } from '@polymer/lit-element'

export class MyTitle extends LitElement {
  constructor() {
    super()
  }
  render(){
    return html`
      <h1>
        Kitchen Sink 🍔 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)