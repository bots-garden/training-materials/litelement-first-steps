# 🇬🇧LitElement - First steps

## Resources

- https://lit-element.polymer-project.org/
  - > A simple base class for creating fast, lightweight web components
- https://lit-html.polymer-project.org/
  - > An efficient, expressive, extensible HTML templating library for JavaScript 
- https://github.com/LarsDenBakker/lit-html-examples

- https://43081j.com/2018/08/future-of-polymer

- https://polymer.slack.com/archives/C03PF4L4L/p1525794571000791
central state store (like redux)  unistore or others
communication between *sibling* elements

- https://stackoverflow.com/questions/tagged/lit-element

## Questions

- Is it Better to do start with only one component from the `index.html` page and then all the application is written in JavaScript


## TODO

- learn `polymer.json`