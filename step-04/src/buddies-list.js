import { LitElement, html } from '@polymer/lit-element'
import {style} from './main-styles.js';

import { channels } from './bus.js'


export class BuddiesList extends LitElement {

  constructor() {
    super()
    this.buddies = [
      "🦊 Foxy",
      "🦁 Leo",
      "🐯 Tigrou"
    ]
    channels.push(this)
    console.log(channels)

  }
  render(){
    return html`
      ${style}
      <div>
        ${this.buddies.map(buddy => 
          html`<h2 class="subtitle">${buddy}</h2>`
        )}
      </div>
    `
  }
}
customElements.define('buddies-list', BuddiesList)